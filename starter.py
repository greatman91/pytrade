import pywinauto
#import pyautogui
import time
import configparser

class Starter:
	def __init__(self):
		config = configparser.ConfigParser()
		config.read('conf/conf.ini')
		self.pw = config['CYBOS']['pw']


	def launch(self):
		app = pywinauto.Application()
		app.start(r'C:/Daishin/Starter/ncStarter.exe /prj:cp')
		print('cp load done')
		time.sleep(3)
		#cybos 보안프로그램 사용 X
		try:		
			dlg_nc = pywinauto.timings.wait_until_passes(20,0.5, lambda: app.connect(title="ncStarter")).Dialog
			yes_btn = dlg_nc.Button
			yes_btn.click()
			time.sleep(1)
		except Exception as e:
			print(e)
			pass

		dlg_family = pywinauto.timings.wait_until_passes(100,0.5,lambda: app.connect(title='대신증권 CYBOS FAMILY')).Dialog
		yes_btn = dlg_family.Button
		yes_btn.click()

		#pyautogui.typewrite('\n', interval=1)
		#print('enter done')

		dlg = pywinauto.timings.wait_until_passes(100, 0.5, lambda: app.connect(title="CYBOS Starter")).Dialog
		print('dlg done')

		# pass edit
		pass_edit = dlg.Edit2
		pass_edit.set_focus()
		pass_edit.type_keys(self.pw)
		print('pw done')

		# login
		btn = dlg.Button
		btn.click()
		print('login click')

if __name__ == '__main__':
	starter = Starter()
	starter.launch()


