from slacker import Slacker
from pusher.base_pusher import Pusher
import configparser

class PushSlack(Pusher):

    def __init__(self):
        config = configparser.ConfigParser()
        config.read('conf/conf.ini')
        token = config['SLACK']['token']
        self.slack = Slacker(token)
        self.send_message(message="Slack initiated")

    def send_message(self, thread="#koa", message=None):
        self.slack.chat.post_message(thread, message)
