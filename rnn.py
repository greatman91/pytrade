import pandas as pd
from pandas import DataFrame
import numpy as np
from cybos import Cybos
from sklearn.preprocessing import MinMaxScaler
from keras.layers import LSTM
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils
import keras

import matplotlib.pyplot as plt


class LossHistory(keras.callbacks.Callback):
	def init(self):
		self.losses = []

	def on_epoch_end(self, batch, logs={}):
		self.losses.append(logs.get('loss'))
class LSTM:
	def __init__(self):
		self.cybos = Cybos()
		self.sc = MinMaxScaler()
		self.losshistory= LossHistory()
	def getdata(self, code):
		frame=self.cybos.stockChart(code = code, chart = 'D', minute = 1, ea=161)
		frame['close'] = pd.to_numeric(frame['close'], errors='coerce')
		returns = frame['close'].pct_change().dropna()
		return returns

	def seq2dataset(self, seq, window_size):
		dataset = []
		for i in range(len(seq)-window_size):
			subset = seq[i:(i+window_size+1)]
			dataset.append([item for item in subset])

		print(np.percentile(dataset, 50))
		print(np.percentile(dataset, 75))
		for i in range(len(dataset)):
			tmp = dataset[i][window_size]
			#if tmp < np.percentile(dataset, 25):
			#	dataset[i][window_size] = 1
			if tmp < np.percentile(dataset, 50):
				dataset[i][window_size] = 0
			#elif tmp < np.percentile(dataset, 75):
			#	dataset[i][window_size] = 1
			else:
				dataset[i][window_size] = 1
			"""
			for item in subset:
				if item < np.percentile(seq, 75):
					tmp.append(0)
				#elif item < np.percentile(seq, 50):
				#	tmp.append(1)
				#elif item < np.percentile(seq, 75):
				#	tmp.append(2)
				else:
					tmp.append(1)
			dataset.append(tmp)
			"""
		print(dataset)
		return np.array(dataset)

	def train(self, code):
		data=self.getdata(code=code)
		#re_data = data.to_frame()
		#data_1 = self.sc.fit_transform(data.values)
		#sc_df= pd.DataFrame(data_1)
		window_size = 20
		dataset = self.seq2dataset(data, window_size=window_size)
		print(len(dataset))
		val = 100
		split = 100
		x_train = dataset[:split,0:window_size]
		y_train = dataset[:split,window_size]
		x_train = x_train
		#x_train = np.reshape(x_train, (split,10,1))
		y_train = np_utils.to_categorical(y_train)
		x_test = dataset[split:,0:window_size]
		x_test = x_test
		y_test = dataset[split:,window_size]
		y_test = np_utils.to_categorical(y_test)

		one_hot_vec_size = y_train.shape[1]
		model = Sequential()
		#model.add(LSTM(128,batch_input_shape=(20,10,1)))
		model.add(Dense(128, input_dim=window_size, activation= 'relu'))
		model.add(Dropout(0.2))
		model.add(Dense(128,activation='relu'))
		model.add(Dropout(0.2))
		model.add(Dense(128,activation='relu'))
		model.add(Dropout(0.2))
		model.add(Dense(one_hot_vec_size, activation = 'softmax'))
		model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

		model.summary()
		self.losshistory.init()

		model.fit(x_train, y_train, epochs=200, batch_size=20, verbose=2, shuffle=False, callbacks=[self.losshistory]) #validation_steps = 20) # 50 is X.shape[0]
		model.reset_states()

		score = model.evaluate(x_test, y_test, batch_size=20)
		print(score)
		y_hat = model.predict(x_test)
		y_test = np.argmax(y_test, axis=1)
		df_y = DataFrame(y_test, columns=['test'])
		y_hat = np.argmax(y_hat, axis=1)
		df_hat = DataFrame(y_hat, columns =['predict'])
		df = pd.concat([df_hat, df_y], axis=1)
		print(df)
		plt.plot(self.losshistory.losses)
		plt.ylabel('loss')
		plt.xlabel('epoch')
		plt.legend(['train'], loc='upper left')
		plt.show()

if __name__ == '__main__':
	runner = LSTM()
	runner.train('A032820')



