import sqlite3
import pandas as pd

#insert delete update select
class SqliteDB:
	def __init__(self):
		self.con = sqlite3.connect("c:/Users/pahjh/Desktop/KOA/db/cond.db")
		self.cursor = self.con.cursor()

	def get_items(self):
		sqltxt = "SELECT DISTINCT Code FROM inv" #WHERE Date = "+yesterday         #yesterday = date.today() - timedelta(days=1)        #yesterday = yesterday.strftime('%Y-%m-%d')
		#self.cursor.execute(sqltxt)
		res= pd.read_sql(sqltxt, self.con, index_col=None)
		res = res['Code'].tolist()
		return res

	def delete_item(self, code):
		sqltxt = "DELETE FROM inv WHERE Code =?"
		self.cursor.execute(sqltxt, (code,))
		self.con.commit()

	def insert_item(self, date, code):
		task = (date,code)
		sqltxt = "INSERT INTO inv VALUES(?,?)"
		self.cursor.execute(sqltxt, task)
		self.con.commit()
		return self.cursor.lastrowid


if __name__ == "__main__":
	sqlite = SqliteDB()
	print(type(sqlite.con))