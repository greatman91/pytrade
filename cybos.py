import sys, time
import win32com.client
import ctypes
import datetime
from pandas import DataFrame
import pandas as pd
import numpy as np
from numpy import mean, absolute
from db.sqlite_dbhandler import SqliteDB
from datetime import date
from scipy.stats import normaltest
from scipy.stats import skew, kurtosis
from starter import Starter
#import matplotlib.pyplot as plt
#import matplotlib as mpl
#from mpl_toolkits.mplot3d import Axes3D


class Cybos:
    def __init__(self):
        self.Starter = Starter()
        self.Starter.launch()
        self.instCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        self.instStockChart = win32com.client.Dispatch("CpSysDib.StockChart")
        self.inStockMst = win32com.client.Dispatch("dscbo1.StockMst")
        #self.sqlite = SqliteDB()
        self.check_connect() #location fixed
        
    def check_connect(self):
        time.sleep(30)
        if ctypes.windll.shell32.IsUserAnAdmin():
            print('Normal: Process executed as an admin.')
        else:
            raise Exception('Error : Need Administrative rights.')

        self.check_CP()

    def check_CP(self, type = 1):
        if type == 0:
            self.instCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        else:
            pass

        if self.instCpCybos.IsConnect == 0:
            print("Cybos not connected")
            self.Starter.launch()
            time.sleep(30)
            self.instCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        else:
            print("Cybos Connected")


    # code type 'A000000'
    def stockChart(self, code, chart = 'm', ea = 80, minute = 3):# min = 3 ea 66
        self.instStockChart.SetInputValue(0, code)
        self.instStockChart.SetInputValue(1, ord('2'))
        self.instStockChart.SetInputValue(4, ea)
        self.instStockChart.SetInputValue(5, [0,1, 2, 3, 4, 5, 8])
        self.instStockChart.SetInputValue(6, ord(chart))
        self.instStockChart.SetInputValue(7, minute)#min
        self.instStockChart.SetInputValue(9, ord('1'))

        self.instStockChart.BlockRequest()

        rqStatus = self.instStockChart.GetDibStatus()
        rqRet = self.instStockChart.GetDibMsg1()
        #print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            print("통신상태", rqStatus, rqRet)
            #exit()

        len = self.instStockChart.GetHeaderValue(3)#candle ea.

        caller = {'date':[],'time':[],'open':[],'high':[],'low':[],'close':[],'volume':[]}

        for i in range(len):
            caller['date'].append(self.instStockChart.GetDataValue(0,i))
            caller['time'].append(self.instStockChart.GetDataValue(1,i))
            caller['open'].append(self.instStockChart.GetDataValue(2, i))
            caller['high'].append(self.instStockChart.GetDataValue(3, i))
            caller['low'].append(self.instStockChart.GetDataValue(4, i))
            caller['close'].append(self.instStockChart.GetDataValue(5, i))
            caller['volume'].append(self.instStockChart.GetDataValue(6, i))
            #caller['lv'].append(self.instStockChart.GetDataValue(7,i))

        frame = DataFrame(caller)
        frame = frame.sort_values(by=['date','time'], ascending=True)
        frame['open'] = pd.to_numeric(frame['open'],errors = 'coerce')
        frame['high'] = pd.to_numeric(frame['high'], errors='coerce')
        frame['low'] = pd.to_numeric(frame['low'], errors='coerce')
        frame['close'] = pd.to_numeric(frame['close'], errors='coerce')
        frame['volume'] = pd.to_numeric(frame['volume'], errors='coerce')
        frame['returns'] = np.log(frame['close']/frame['close'].shift())
        frame['c_h'] = np.where(frame['close']>=frame['open'],np.log(frame['close']/frame['high']),\
        np.log(frame['open']/frame['high']))
        frame['h_l'] = np.log(frame['high']/frame['low'])
        frame['noise'] = np.where(frame['h_l']>=frame['returns'].rolling(window=30).std(),\
        1-abs(frame['open']-frame['close']+frame['close']*0.001)/(frame['high']-frame['low']+frame['close']*0.001),0.5)
        frame['amount'] = frame['close']*frame['volume']
        return frame
        """
    def update_sqlite(self):
        items=self.sqlite.get_items()
        for item in items:
            if item == None:
                continue
            item = "A"+item
            lv = self.get_change(item)
            print(item, " : ",lv)
            if lv < -0.005:
                self.sqlite.delete_item(item[1:])
                print(item," is eliminated")
        """

    def get_change(self, code):
        self.inStockMst.SetInputValue(0,code)
        self.inStockMst.BlockRequest()
        cl = self.inStockMst.GetHeaderValue(11)
        if cl == 0:
            return 0
        else:
            if cl < 0.985*self.inStockMst.GetHeaderValue(10):
                lv = -100
            else:
                lv = self.inStockMst.GetHeaderValue(12)/self.inStockMst.GetHeaderValue(10)
            return lv

    def get_vol_value(self,code):
        self.inStockMst.SetInputValue(0,code)
        self.inStockMst.BlockRequest()

        rqStatus = self.inStockMst.GetDibStatus()
        rqRet = self.inStockMst.GetDibMsg1()
        if rqStatus != 0:
            print(rqRet)
            #exit()

        res = self.inStockMst.GetHeaderValue(19)
        return res

    def test(self, code):
        frame=self.stockChart(code, minute=3)
        frame['open'] = pd.to_numeric(frame['open'],errors = 'coerce')
        frame['high'] = pd.to_numeric(frame['high'], errors='coerce')
        frame['close'] = pd.to_numeric(frame['close'], errors='coerce')
        frame['low'] = pd.to_numeric(frame['low'], errors='coerce')
        #frame['c_h'] = np.log(frame['close']/frame['high'])
        frame['returns'] = np.log(frame['close']/frame['close'].shift())
        frame['noise'] = 1-abs(frame['open']-frame['close'])/(0.000001+frame['high']-frame['low'])
        frame['amount'] = frame['close']*frame['volume']/100000000
        no_k = pd.Series((frame['noise'].rolling(window=20).mean()))
        frame['c_h'] = np.where(frame['close']>=frame['open'],np.log(frame['close']/frame['high']),np.log(frame['open']/frame['high']))

        frame['mean5vol'] = frame['amount'].rolling(window=5).mean()
        print(frame.iloc[20:])
if __name__ == '__main__':
    cybos = Cybos()
    cybos.test('A050860')
    """
    stocks = ['A032820','A051490','A096240', 'A156100']
    for item in stocks:
        print(item)
        cybos.test(item)
        #d1=cybos.day_drift(code=item)
        #print(d1)
        #frame['returns'] = np.log(frame['close']/frame['close'].shift())
        #frame['vol_ch'] =np.log(frame['vol']/frame['vol'].shift())
        #frame['target'] = frame['returns'].shift()
        #print(frame.iloc[-10:])
        #print(frame.iloc[-2:])
        #pd.DataFrame(tmp_k.values[::-1],tmp_k.index, tmp_k.columns)
        #frame['rsi'] =pd.DataFrame(tmp_k.values[::-1],tmp_k.index, tmp_k.columns)
        #print(frame[20:])
        #plt.plot(frame['time'],tmp_k)
        
        fig = plt.figure()#
        ax = fig.gca(projection='3d')
        ax.scatter(frame['returns'].iloc[-55:], frame['vol_ch'].iloc[-55:], frame['target'].iloc[-55:], label='test')      # 위에서 정의한 x,y,z 가지고 그래프그린거다.
        ax.set_xlabel('X Label')
        ax.set_ylabel('Y Label')
        ax.set_zlabel('Z Label')
        plt.show()
        """

                #vol_osc = pd.Series(100*(frame['vol'].rolling(window=5).mean() - frame['vol'].rolling(window=15).mean())/(frame['vol'].rolling(window=5).mean())+0.000001) 
        #if vol_osc.iloc[-1] >= -20 and type =='b':
        #    pass
        #elif type =='s':
        #    pass
        #else:
        #    return [-1,0,0,0,0,0,0,0,0,0]