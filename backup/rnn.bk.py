import pandas as pd
from pandas import DataFrame
import numpy as np
from cybos import Cybos
from sklearn.preprocessing import MinMaxScaler
from keras.layers import LSTM
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import np_utils
import keras

import matplotlib.pyplot as plt


class LossHistory(keras.callbacks.Callback):
    def init(self):
        self.losses = []
        
    def on_epoch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))

class LSTM:
	def __init__(self):
		self.cybos = Cybos()
		self.sc = MinMaxScaler()
		self.losshistory= LossHistory()
	def getdata(self, code):
		frame=self.cybos.stockChart(code = code, chart = 'D', minute = 1, ea=111)
		frame['close'] = pd.to_numeric(frame['close'], errors='coerce')
		returns = frame['close'].pct_change().dropna()
		return returns

	def seq2dataset(self, seq, window_size):
	    dataset = []
	    for i in range(len(seq)-window_size):
	        subset = seq[i:(i+window_size+1)]
	        #dataset.append([item for item in subset])
	        for item in subset:
		        if item < np.percentile(dataset, 10):
		        	dataset.append(1)
		        elif item < np.percentile(dataset, 25):
		        	dataset.append(2)
		        elif item < np.percentile(dataset, 50):
		        	dataset.append(3)
		        elif item < np.percentile(dataset, 75):
		        	dataset.append(4)
		        elif item < np.percentile(dataset, 90):
		        	dataset.append(5)
		        else:
		        	dataset.append(6)
		        	
		"""
	    print(np.percentile(dataset, 90))
	    print(np.percentile(dataset, 75))
	    print(np.percentile(dataset, 50))
	    print(np.percentile(dataset, 25))
	    print(np.percentile(dataset, 10))

	    for i in range(len(dataset)):
	    	tmp = dataset[i][window_size]
	    	if tmp < np.percentile(dataset, 10):
	    		dataset[i][window_size] = 1
	    	elif tmp < np.percentile(dataset, 25):
	    		dataset[i][window_size] = 2
	    	elif tmp < np.percentile(dataset, 50):
	    		dataset[i][window_size] = 3
	    	elif tmp < np.percentile(dataset, 75):
	    		dataset[i][window_size] = 4
	    	elif tmp < np.percentile(dataset, 90):
	    		dataset[i][window_size] = 5
	    	else:
	    		dataset[i][window_size] = 6
		"""
	    print(dataset)
		return np.array(dataset)

	def train(self, code):
		data=self.getdata(code=code)
		#re_data = data.to_frame()
		#data_1 = self.sc.fit_transform(data.values)
		#sc_df= pd.DataFrame(data_1)
		dataset = self.seq2dataset(data, window_size=10)
		print(len(dataset))
		x_train = dataset[:80,0:10]
		y_train = dataset[:80,10]
		y_train = np_utils.to_categorical(y_train)
		x_test = dataset[80:,0:10]
		y_test = dataset[80:,10]
		y_test = np_utils.to_categorical(y_test)

		one_hot_vec_size = y_train.shape[1]
		model = Sequential()
		model.add(Dense(128, input_dim=10, activation= 'relu'))
		model.add(Dense(128,activation='relu'))
		model.add(Dense(one_hot_vec_size, activation = 'softmax'))
		model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

		model.summary()
		self.losshistory.init()

		model.fit(x_train, y_train, epochs=100, batch_size=20, verbose=2, shuffle=False, callbacks=[self.losshistory]) #validation_steps = 20) # 50 is X.shape[0]
		model.reset_states()

		score = model.evaluate(x_test, y_test, batch_size=20)
		print(score)
		y_hat = model.predict(x_test)
		df_y = DataFrame(y_test)
		df_hat = DataFrame(y_hat)
		df = pd.concat([df_hat, df_y], axis=1)
		print(df)
		plt.plot(self.losshistory.losses)
		plt.ylabel('loss')
		plt.xlabel('epoch')
		plt.legend(['train'], loc='upper left')
		plt.show()

if __name__ == '__main__':
	runner = LSTM()
	runner.train('A001550')



