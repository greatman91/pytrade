"""
QtDesigner로 만든 UI와 해당 UI의 위젯에서 발생하는 이벤트를 컨트롤하는 클래스

last edit: 2018.02.20
"""

import sys, time
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QTableWidget, QTableWidgetItem
from PyQt5.QtCore import Qt, QTimer, QTime
from PyQt5 import uic
from Kiwoom import Kiwoom, ParameterTypeError, ParameterValueError, KiwoomProcessingError, KiwoomConnectError
from cybos import Cybos
import datetime
from pandas import DataFrame
import pandas as pd
import numpy as np
import threading

ui = uic.loadUiType("pytrader.ui")[0]

class MyWindow(QMainWindow, ui):

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.show()

        self.kiwoom = Kiwoom()
        self.kiwoom.commConnect()
        self.cybos = Cybos()

        self.server = self.kiwoom.getLoginInfo("GetServerGubun")

        if len(self.server) == 0 or self.server != "1":
            self.serverGubun = "Real"
        else:
            self.serverGubun = "Simulated"
        self.codeList = self.kiwoom.getCodeList("0")

        # 메인 타이머
        self.timer = QTimer(self)
        self.timer.start(1000)
        self.timer.timeout.connect(self.timeout)

        self.setAccountComboBox()
        self.codeLineEdit.textChanged.connect(self.setCodeName)
        self.orderBtn.clicked.connect(self.sendOrder)
        self.inquiryBtn.clicked.connect(self.inquiryBalance)
        self.inquiryBtn_2.clicked.connect(self.setAutomatedStocks)
        self.pushButton.clicked.connect(self.flipAuto)
        # 자동 주문
        # 자동 주문을 활성화 하려면 True로 설정
        self.isAutomaticOrder = True
        self.algoStart = False
        self.flag = 0
        self.stockCode = ""
        #self.inquiryBalance()
        # 자동 선정 종목 리스트 테이블 설정
        #self.setAutomatedStocks()

    def timeout(self):
        """ 타임아웃 이벤트가 발생하면 호출되는 메서드 """

        # 어떤 타이머에 의해서 호출되었는지 확인
        sender = self.sender()

        # 메인 타이머
        if id(sender) == id(self.timer):
            currentTime = QTime.currentTime().toString("hh:mm:ss")
            automaticOrderTime = QTime.currentTime().toString("hhmm")

            # 상태바 설정
            state = ""

            if self.kiwoom.getConnectState() == 1:

                state = self.serverGubun + " - 서버 연결중"
            else:
                state = "서버 미연결"

            self.statusbar.showMessage("현재시간: " + currentTime + " | " + state)

            # 자동 주문 실행
            # 1100은 11시 00분을 의미합니다.
            if self.algoStart == False:
                if self.isAutomaticOrder and int(automaticOrderTime) >= 900 and int(automaticOrderTime)<=1530:
                    self.inquiryBalance()
                    time.sleep(2)
                    self.algoStart = True
                    self.automaticOrder()
                    #t = threading.Thread(target=self.automaticOrder)
                    #t.daemon = False
                    #t.start()
                else:
                    #self.algoStart = False    
                    pass

            # log
            if self.kiwoom.msg:
                self.logTextEdit.append(self.kiwoom.msg)
                self.kiwoom.msg = ""

        # 실시간 조회 타이머

    def setCodeName(self):
        """ 종목코드에 해당하는 한글명을 codeNameLineEdit에 설정한다. """

        code = self.codeLineEdit.text()

        if code in self.codeList:
            codeName = self.kiwoom.getMasterCodeName(code)
            self.codeNameLineEdit.setText(codeName)

    def setAccountComboBox(self):
        """ accountComboBox에 계좌번호를 설정한다. """

        try:
            cnt = int(self.kiwoom.getLoginInfo("ACCOUNT_CNT"))
            accountList = self.kiwoom.getLoginInfo("ACCNO").split(';')
            self.accountComboBox.addItems(accountList[0:cnt])
        except (KiwoomConnectError, ParameterTypeError, ParameterValueError) as e:
            self.showDialog('Critical', e)

    def sendOrder(self, orderType, code):
        """ 키움서버로 주문정보를 전송한다. """

        orderTypeTable = {'신규매수': 1, '신규매도': 2, '매수취소': 3, '매도취소': 4}
        hogaTypeTable = {'지정가': "00", '시장가': "03"}

        account = self.accountComboBox.currentText()
        #orderType = orderTypeTable[self.orderTypeComboBox.currentText()]
        #code = self.codeLineEdit.text()
        #hogaType = hogaTypeTable[self.hogaTypeComboBox.currentText()]
        #price = self.priceSpinBox.value()
        #qty = self.qtySpinBox.value()
        hogaType = "00"
        self.kiwoom.setInputValue("종목코드", code)
        self.kiwoom.commRqData("주식호가요청", "opt10004", 0, "0000")
        self.kiwoom.disconnectRealData("0000")
        if orderType == 1:
            price = self.kiwoom.ask
            qty = int(self.kiwoom.cash/price) 
        elif orderType == 2:
            price = self.kiwoom.bid
            qty = int(self.stockQty)  

        try:
            self.kiwoom.sendOrder("주문", "0101", account, orderType, code, qty, price, hogaType, "")

        except (ParameterTypeError, KiwoomProcessingError) as e:
            self.showDialog('Critical', e)

    def inquiryBalance(self):
        """ 예수금상세현황과 계좌평가잔고내역을 요청후 테이블에 출력한다. """


        try:
            # 예수금상세현황요청
            self.kiwoom.setInputValue("계좌번호", self.accountComboBox.currentText())
            self.kiwoom.setInputValue("비밀번호", "0000")
            self.kiwoom.commRqData("예수금상세현황요청", "opw00001", 0, "2000")

            # 계좌평가잔고내역요청 - opw00018 은 한번에 20개의 종목정보를 반환
            self.kiwoom.setInputValue("계좌번호", self.accountComboBox.currentText())
            self.kiwoom.setInputValue("비밀번호", "0000")
            self.kiwoom.commRqData("계좌평가잔고내역요청", "opw00018", 0, "2000")

            while self.kiwoom.inquiry == '2':
                time.sleep(0.2)

                self.kiwoom.setInputValue("계좌번호", self.accountComboBox.currentText())
                self.kiwoom.setInputValue("비밀번호", "0000")
                self.kiwoom.commRqData("계좌평가잔고내역요청", "opw00018", 2, "2")

        except (ParameterTypeError, ParameterValueError, KiwoomProcessingError) as e:
            self.showDialog('Critical', e)

        # accountEvaluationTable 테이블에 정보 출력
        if self.kiwoom.cash < 5000:
            self.flag = 2
            if self.kiwoom.opw00018Data['stocks'] != []:
                self.stockCode = self.kiwoom.opw00018Data['stocks'][0][0]
                self.stockQty = self.kiwoom.opw00018Data['stocks'][0][2]
                print("cash : ",self.kiwoom.cash)
                print("holding : ",self.stockCode," qty : ",self.stockQty)
        item = QTableWidgetItem(self.kiwoom.opw00001Data)   # d+2추정예수금
        item.setTextAlignment(Qt.AlignVCenter | Qt.AlignRight)
        self.accountEvaluationTable.setItem(0, 0, item)

        for i in range(1, 6):
            item = QTableWidgetItem(self.kiwoom.opw00018Data['accountEvaluation'][i-1])
            item.setTextAlignment(Qt.AlignVCenter | Qt.AlignRight)
            self.accountEvaluationTable.setItem(0, i, item)

        self.accountEvaluationTable.resizeRowsToContents()

        # stocksTable 테이블에 정보 출력
        cnt = len(self.kiwoom.opw00018Data['stocks'])
        self.stocksTable.setRowCount(cnt)

        for i in range(cnt):
            row = self.kiwoom.opw00018Data['stocks'][i]

            for j in range(len(row)):
                item = QTableWidgetItem(row[j])
                item.setTextAlignment(Qt.AlignVCenter | Qt.AlignRight)
                self.stocksTable.setItem(i, j, item)

        self.stocksTable.resizeRowsToContents()

        # 데이터 초기화
        self.kiwoom.opwDataReset()


    # 경고창
    def showDialog(self, grade, error):
        gradeTable = {'Information': 1, 'Warning': 2, 'Critical': 3, 'Question': 4}

        dialog = QMessageBox()
        dialog.setIcon(gradeTable[grade])
        dialog.setText(error.msg)
        dialog.setWindowTitle(grade)
        dialog.setStandardButtons(QMessageBox.Ok)
        dialog.exec_()

    def getConditionItems(self):
        self.kiwoom.getConditionLoad()
        self.kiwoom.sendCondition("0150","Invincible", 2, 0)

    def setAutomatedStocks(self):
        automatedStocks = []

#        try:
        self.getConditionItems()
        automatedStocks = self.kiwoom.condList
#        except Exception as e:
#            e.msg = "setAutomatedStocks() 에러"
#            self.showDialog('Critical', e)
#            return

        # 테이블 행수 설정
        cnt = len(automatedStocks)
        self.automatedStocksTable.setRowCount(cnt)

        # 테이블에 출력
        for i in range(cnt):
            stock = automatedStocks[i]
            j=0
            time.sleep(0.3)
            name = self.kiwoom.getMasterCodeName(stock)#[j].rstrip())
            item = QTableWidgetItem(name)
            item.setTextAlignment(Qt.AlignVCenter | Qt.AlignCenter)
            self.automatedStocksTable.setItem(i,j,QTableWidgetItem(stock))
            self.automatedStocksTable.setItem(i, j+1, item)

        self.automatedStocksTable.resizeRowsToContents()

    def automaticOrder(self):
        while (self.algoStart):# and int(automaticOrderTime) >= 900 and int(automaticOrderTime)<=1530):
            if self.flag ==0:
                print("bid session")
                self.buy_scenario()
                #t1.start()
                
            else:
                print("ask session")
                try:
                    self.sell_scenario()
                    #t2.start()
                except Exception:
                    continue


        # 잔고및 보유종목 디스플레이 갱신
        self.inquiryBalance()


    def buy_scenario(self):
        while(self.flag==0):
            self.getConditionItems()
            time.sleep(0.1)
            stocks = self.kiwoom.condList
            print(stocks)
            self.printCount()
            if stocks == []:
                print('condList is empty')
                continue
            for item in stocks: 
                self.stockCode = "A"+item
                print("-------------------------------------------------------------------------------")
                cci_val = self.cybos.cci(item, 50)
                print(self.stockCode," : ",cci_val)
                #if cci_val[0]> 100 and  cci_val[1]>0 and cci_val[2]>100:
                #    print(self.stockCode)
                #    self.flag = 1
                #    break
                if cci_val[0]>100 and cci_val[1]>0 and cci_val[2]<100:
                    print(self.stockCode)
                    self.flag = 2
                    break
                time.sleep(5)
                 
        self.sendOrder(1, self.stockCode)

 
    def sell_scenario(self):
        i = 0
        while(self.flag > 0):
            i = i+1
            self.printCount()
            cci_val=self.cybos.cci(self.stockCode, 50)
            if cci_val[0]<100 and  cci_val[1]<0 and cci_val[2]>100 and self.flag==2: #down stream
                self.flag = 0
                break
            elif cci_val[1]<0 and self.flag==1:
                self.flag = 0
                break    
            #elif cci_val[0]>100 and self.flag==1:
            #    self.flag = 2   
            print(i," waiting for selling")
            print(self.stockCode," : ",cci_val)
            
                        #print(mas_val)
        self.sendOrder(2, self.stockCode)
        print("sold")

    def flipAuto(self):
        if self.isAutomaticOrder == False:
            self.isAutomaticOrder = True
            #self.algoStart = True
            self.logTextEdit.append("Auto trade on")
            #for test
            self.inquiryBalance()
            test_t = threading.Thread(target=self.sell_scenario)
            test_t.daemon = False
            test_t.start()
            
        else:
            self.isAutomaticOrder = False
            self.logTextEdit.append("Auto trade off")
    
    def printCount(self):
        for i in range(1,101):
            print(i, "percent complete         \r",end='')
            time.sleep(0.59)    


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWindow = MyWindow()
    sys.exit(app.exec_())