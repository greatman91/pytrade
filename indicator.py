import sys, time
import ctypes
import datetime
from pandas import DataFrame
import pandas as pd
import numpy as np
from numpy import mean, absolute
from datetime import date
from scipy.stats import normaltest
from scipy.stats import skew, kurtosis

class Indicator():

    def RSI(self, m_Df):#m_Df -->close
        U = pd.DataFrame(np.where(m_Df.diff(1) > 0, m_Df.diff(1), 0.000001))#0.000006626
        D = pd.DataFrame(np.where(m_Df.diff(1) < 0, m_Df.diff(1) *(-1), 0.000001))#0.000003374
        #AU = pd.DataFrame(U).rolling( window=m_N, min_periods=m_N).mean()
        #AD = pd.DataFrame(D).rolling( window=m_N, min_periods=m_N).mean()
        AU = pd.DataFrame.ewm(U,alpha = 1/14).mean()
        AD = pd.DataFrame.ewm(D,alpha = 1/14).mean()
        RSI = AU.div(AD+AU) *100
        RSI_signal = pd.DataFrame(RSI.rolling(window=9).mean())
        #RSI_signal = pd.Series.ewm(RSI, span=9).mean()
        return RSI_signal

    def get_drift(self,m_Df):
        mu = np.mean(m_Df['returns'])
        sigma = np.std(m_Df['returns'])
        drift = mu - 0.5*(sigma**2)
        return drift

    def get_scaled_data(self, data, type = 0):
        k2, p = normaltest(data)
        scaled = data.values
        #if delete below, the result of normal test fluctuate hard.
        if p < 0.1:    
            skewness = skew(data)
            i = 1
            while(skewness < -0.5 or skewness > 0.5): 
                scaled=np.sqrt(np.abs(data))*np.sign(data)
                data = scaled
                skewness = skew(scaled)
                #print("scaled skew ",i," : ", skewness)
                if abs(skewness) >= abs(skew(data)):
                    break
                else:
                    pass
                if i >= 3:
                    break
                i=i+1

        else:
            pass
        
        df=DataFrame(scaled)
        k2, p = normaltest(df)
        #print("df norm test : " ,p)
        #print("std : ", np.std(df))
        if type == 0:
            minus = 0.8
        else:
            minus = 0.4
        if np.std(df).item() == 0:
            exp_std = 1
        else:
            log_std = np.log10(np.std(df))*-1-minus
            exp_std=np.where(log_std>=1,log_std,1)
        #exp_std = np.log(np.std(df))
        #print("exp_std : ", exp_std)
        add_df = pd.DataFrame(np.random.normal(np.mean(df),exp_std*np.std(df),60),columns=['scale'])
        df = df.rename({df.columns[0]:"scale"},axis=1)
        add_df = add_df.append(df, ignore_index=True)
        k2, p = normaltest(add_df)
        #print("add norm test : ", p)
        return add_df

    def get_zscore(self, df):
        mu = np.mean(df)
        sigma = np.std(df)
        scaled = (df-mu)/sigma
        return scaled#iloc[i].item()

    def check_ewm(self,m7, m20):
        ea = 20
        spread = (m7.iloc[ea:]-m20.iloc[ea:])/m20.iloc[ea:]
        if spread.iloc[-1]>0 and spread.iloc[-2] >0:  #0.0001
            return 'g'
        elif spread.iloc[-1]<0 and spread.iloc[-2]<0:
            return 'r'
        else:
            return 'y'

    def get_indis(self,df):
        res = []
        _7mean = pd.Series.ewm(df['close'], span=7).mean()
        _20mean = pd.Series.ewm(df['close'], span=20).mean()
        _7vol = pd.Series.ewm(df['volume'],span=7).mean()
        _20vol = pd.Series.ewm(df['volume'],span=20).mean()
        delta = []
        for i in range(-1,-4,-1):
            delta.append((_7mean.iloc[i]-_7mean.iloc[i-1])/_7mean.iloc[i-1])
        res.append(_7mean)
        res.append(_20mean)
        res.append(_20vol)
        res.append(delta)
        scaled_cl = self.get_scaled_data(df['returns'].iloc[-31:],type=0)
        scaled_ch = self.get_scaled_data(df['c_h'].iloc[-31:],type=1)
        scaled_n = self.get_scaled_data(df['noise'].iloc[-31:],type=0)
        zscore = self.get_zscore(scaled_cl)
        zscore_ch = self.get_zscore(scaled_ch)
        zscore_n = self.get_zscore(scaled_n)
        value_check = pd.value_counts(zscore_ch['scale'].iloc[-30:].values, sort=True)
        value_check_re = pd.value_counts(zscore['scale'].iloc[-30:].values, sort=True)
        if value_check.iloc[0]>15 or len(value_check)<4:
            for i in range(-1,-5,-1):
                if zscore_ch['scale'].iloc[i]<-2.7:
                    break
                zscore_ch['scale'].iloc[i] = 0
        if value_check_re.iloc[0]>15 or len(value_check_re)<5:
            for i in range(-1,-5,-1):
                if zscore['scale'].iloc[i]<-2.7:
                    break
                zscore['scale'].iloc[i] = 0
        res.append(zscore['scale'])
        res.append(zscore_ch['scale'])
        sigma = np.std(df['returns'].iloc[-30:])
        res.append(sigma)
        res.append(zscore_n['scale'])
        #print("sigma : ", sigma)
        return res
        
    def get_baseline(self, price, type='n'):
        price_str = str(price)
        first_fl = float(price_str[:3])/100
        tmp = first_fl*0.05
        if type =='u' or type =='l':
            base_num = 50
        else:
            base_num = 150

        if price<10000:
            const = base_num
        elif price <100000:
            const = base_num+50
        else:
            const = base_num+150
        res = np.exp(tmp*-1)/const
        if type == 'l':
            return res/2
        return res
    