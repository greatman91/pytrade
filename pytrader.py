"""
QtDesigner로 만든 UI와 해당 UI의 위젯에서 발생하는 이벤트를 컨트롤하는 클래스
version control !!!  update --> KOA studio
"""

import sys, time
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QTableWidget, QTableWidgetItem
from PyQt5.QtCore import Qt, QTimer, QTime
from PyQt5 import uic
from Kiwoom import Kiwoom, ParameterTypeError, ParameterValueError, KiwoomProcessingError, KiwoomConnectError
from cybos import Cybos
import numpy as np
import datetime
from pusher.slack import PushSlack
#from db.sqlite_dbhandler import SqliteDB
#import operator
import warnings
import psutil
from indicator import Indicator

warnings.simplefilter("ignore", UserWarning)
sys.coinit_flags = 2

ui = uic.loadUiType("pytrader.ui")[0]

class MyWindow(QMainWindow, ui):

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.show()

        self.kiwoom = Kiwoom()
        self.kiwoom.commConnect()
        self.cybos = Cybos()
        self.calc = Indicator()
        self.pusher = PushSlack()
        #self.sqlite = SqliteDB()

        self.server = self.kiwoom.getLoginInfo("GetServerGubun")

        if len(self.server) == 0 or self.server != "1":
            self.serverGubun = "Real"
        else:
            self.serverGubun = "Simulated"
        self.codeList = self.kiwoom.getCodeList("0")

        # 메인 타이머
        self.timer = QTimer(self)
        self.timer.start(1000)
        self.timer.timeout.connect(self.timeout)
                # 잔고 및 보유종목 조회 타이머
        self.inquiryTimer = QTimer(self)
        self.inquiryTimer.start(1000*7)
        self.inquiryTimer.timeout.connect(self.timeout)

        self.condautoTimer = QTimer(self)
        self.condautoTimer.start(1000*9)
        self.condautoTimer.timeout.connect(self.timeout)

        self.tradeTimer = QTimer(self)
        self.tradeTimer.start(1000*9)
        self.tradeTimer.timeout.connect(self.timeout)

        self.setAccountComboBox()
        self.codeLineEdit.textChanged.connect(self.setCodeName)
        self.orderBtn.clicked.connect(self.sendOrder)
        self.inquiryBtn.clicked.connect(self.inquiryBalance)
        self.inquiryBtn_2.clicked.connect(self.setAutomatedStocks)
        self.pushButton.clicked.connect(self.flipAuto)


        # 자동 주문을 활성화 하려면 True로 설정
        self.isAutomaticOrder = True
        self.algoStart = False
        self.flag = 0
        self.stockCode = ""
        self.holdings = []
        self.condItems = []
        self.weekday = datetime.datetime.now().weekday()
        #self.sqlres = self.sqlite.get_items()
        self.counts = []
        self.tick = []

    def timeout(self):
        """ 타임아웃 이벤트가 발생하면 호출되는 메서드 """

        # 어떤 타이머에 의해서 호출되었는지 확인
        sender = self.sender()

        # 메인 타이머
        if id(sender) == id(self.timer):
            currentTime = QTime.currentTime().toString("hh:mm:ss")
            automaticOrderTime = QTime.currentTime().toString("hhmm")

            # 상태바 설정
            state = ""

            if self.kiwoom.getConnectState() == 1:

                state = self.serverGubun + " - 서버 연결중"
            else:
                state = "서버 미연결"
                if self.flag == 0:
                    time.sleep(3)
                    self.pusher.send_message(message="Disconnected")
                    self.flag = 1
                    self.kiwoom.commConnect()
                    server = self.kiwoom.getServerGubun()
                    print("server: ", server)
                    print("len: ", len(server))

                    if len(server) == 0 or server != "1":
                        print("실서버 입니다.")
                        self.flag = 0

            self.statusbar.showMessage("현재시간: " + currentTime + " | " + state)

            # 자동 주문 실행
            # 1100은 11시 00분을 의미합니다.
            if self.algoStart == False:
                if self.isAutomaticOrder and int(automaticOrderTime) >= 900 and int(automaticOrderTime)<=1519 and self.weekday<5:
                    self.algoStart = True
                    #self.automaticOrder()

                else:
                    self.algoStart = False    
                    pass

            # log
            if self.kiwoom.msg:
                self.logTextEdit.append(self.kiwoom.msg)
                self.kiwoom.msg = ""

        # 실시간 조회 타이머
        #elif id(sender) == id(self.condautoTimer):
            #self.inquiryBalance()
            #if self.realtimeCheckBox_2.isChecked():
        elif id(sender) == id(self.tradeTimer):
            self.automaticOrder()
        #else:
        #    if self.realtimeCheckBox.isChecked():
        #        self.inquiryBalance()


    def setCodeName(self):
        """ 종목코드에 해당하는 한글명을 codeNameLineEdit에 설정한다. """

        code = self.codeLineEdit.text()

        if code in self.codeList:
            codeName = self.kiwoom.getMasterCodeName(code)
            self.codeNameLineEdit.setText(codeName)

    def setAccountComboBox(self):
        """ accountComboBox에 계좌번호를 설정한다. """

        try:
            cnt = int(self.kiwoom.getLoginInfo("ACCOUNT_CNT"))
            accountList = self.kiwoom.getLoginInfo("ACCNO").split(';')
            self.accountComboBox.addItems(accountList[0:cnt])
        except (KiwoomConnectError, ParameterTypeError, ParameterValueError) as e:
            self.showDialog('Critical', e)

    def sendOrder(self, orderType, code):
        """ 키움서버로 주문정보를 전송한다. """

        orderTypeTable = {'신규매수': 1, '신규매도': 2, '매수취소': 3, '매도취소': 4}
        hogaTypeTable = {'지정가': "00", '시장가': "03"}

        account = self.accountComboBox.currentText()
        hogaType = "00"
        self.kiwoom.setInputValue("종목코드", code)
        self.kiwoom.commRqData("주식호가요청", "opt10004", 0, "0000")
        self.kiwoom.disconnectRealData("0000")
        print(code," order made")
        if orderType == 1:
            price = self.kiwoom.ask
            if price > self.kiwoom.cash:
                return
            elif price == 0:
                return
            qty = int(self.kiwoom.cash/price)
            if qty < 1:
                return 
        elif orderType == 2:
            #price = self.kiwoom.bid
            price = 0
            qty = int(self.stockQty)
            hogaType = "03"  

        try:
            self.kiwoom.sendOrder("주문", "0101", account, orderType, code, qty, price, hogaType, "")

            self.inquiryBalance()
        except (ParameterTypeError, KiwoomProcessingError) as e:
            self.showDialog('Critical', e)

    def inquiryBalance(self):
        """ 예수금상세현황과 계좌평가잔고내역을 요청후 테이블에 출력한다. """
        #self.inquiryTimer.stop()

        try:
            # 예수금상세현황요청
            self.kiwoom.setInputValue("계좌번호", self.accountComboBox.currentText())
            self.kiwoom.setInputValue("비밀번호", "0000")
            self.kiwoom.setInputValue("비밀번호입력매체구분" , "00")
            self.kiwoom.setInputValue("조회구분","1")
            self.kiwoom.commRqData("예수금상세현황요청", "opw00001", 0, "2000")

            # 계좌평가잔고내역요청 - opw00018 은 한번에 20개의 종목정보를 반환
            self.kiwoom.setInputValue("계좌번호", self.accountComboBox.currentText())
            self.kiwoom.setInputValue("비밀번호", "0000")
            self.kiwoom.setInputValue("비밀번호입력매체구분" , "00")
            self.kiwoom.setInputValue("조회구분" , "2")
            self.kiwoom.commRqData("계좌평가잔고내역요청", "opw00018", 0, "2000")

            while self.kiwoom.inquiry == '2':
                time.sleep(0.2)

                self.kiwoom.setInputValue("계좌번호", self.accountComboBox.currentText())
                self.kiwoom.setInputValue("비밀번호", "0000")
                self.kiwoom.commRqData("계좌평가잔고내역요청", "opw00018", 2, "2")

        except (ParameterTypeError, ParameterValueError, KiwoomProcessingError) as e:
            self.showDialog('Critical', e)

        # accountEvaluationTable 테이블에 정보 출력
        print("cash : ",self.kiwoom.cash)
        self.holdings = []
        if self.kiwoom.opw00018Data['stocks'] != []:
            for i in range(len(self.kiwoom.opw00018Data['stocks'])):
                item = {'stockCode':self.kiwoom.opw00018Data['stocks'][i][0],'stockQty':self.kiwoom.opw00018Data['stocks'][i][2], 'yeild':self.kiwoom.opw00018Data['stocks'][i][6]}
                self.holdings.append(item)
            #self.stockCode = self.kiwoom.opw00018Data['stocks'][0][0]
            #self.stockQty = self.kiwoom.opw00018Data['stocks'][0][2]
            #print("holding : ",self.stockCode," qty : ",self.stockQty)
            print("holdings : ", self.holdings)
        item = QTableWidgetItem(self.kiwoom.opw00001Data)   # d+2추정예수금
        item.setTextAlignment(Qt.AlignVCenter | Qt.AlignRight)
        self.accountEvaluationTable.setItem(0, 0, item)

        for i in range(1, 6):
            item = QTableWidgetItem(self.kiwoom.opw00018Data['accountEvaluation'][i-1])
            item.setTextAlignment(Qt.AlignVCenter | Qt.AlignRight)
            self.accountEvaluationTable.setItem(0, i, item)

        self.accountEvaluationTable.resizeRowsToContents()

        # stocksTable 테이블에 정보 출력
        cnt = len(self.kiwoom.opw00018Data['stocks'])
        self.stocksTable.setRowCount(cnt)

        for i in range(cnt):
            row = self.kiwoom.opw00018Data['stocks'][i]

            for j in range(len(row)):
                item = QTableWidgetItem(row[j])
                item.setTextAlignment(Qt.AlignVCenter | Qt.AlignRight)
                self.stocksTable.setItem(i, j, item)

        self.stocksTable.resizeRowsToContents()

        # 데이터 초기화
        self.kiwoom.opwDataReset()
        #self.inquiryTimer.start(1000*10)


    # 경고창
    def showDialog(self, grade, error):
        gradeTable = {'Information': 1, 'Warning': 2, 'Critical': 3, 'Question': 4}

        dialog = QMessageBox()
        dialog.setIcon(gradeTable[grade])
        dialog.setText(error.msg)
        dialog.setWindowTitle(grade)
        dialog.setStandardButtons(QMessageBox.Ok)
        dialog.exec_()

    def getConditionItems(self, condName = "Invincible", condIndex = 2):
        self.kiwoom.getConditionLoad()
        #print(self.kiwoom.getConditionNameList())  check condlist
        self.kiwoom.sendCondition("0150",condName, condIndex, 0)

    def setAutomatedStocks(self):
        self.condautoTimer.stop()
        automatedStocks = []

        automaticOrderTime = QTime.currentTime().toString("hhmm")
        automaticOrderTime = int(automaticOrderTime)
#        try:
        self.getConditionItems()
        automatedStocks = self.kiwoom.condList
#        except Exception as e:
#            e.msg = "setAutomatedStocks() 에러"
#            self.showDialog('Critical', e)
#            return

        # 테이블 행수 설정
        cnt = len(automatedStocks)
        self.automatedStocksTable.setRowCount(cnt)

        # 테이블에 출력
        for i in range(cnt):
            stock = automatedStocks[i]
            j=0
            time.sleep(0.2)
            name = self.kiwoom.getMasterCodeName(stock)#[j].rstrip())
            item = QTableWidgetItem(name)
            item.setTextAlignment(Qt.AlignVCenter | Qt.AlignCenter)
            self.automatedStocksTable.setItem(i,j,QTableWidgetItem(stock))
            self.automatedStocksTable.setItem(i, j+1, item)

        self.automatedStocksTable.resizeRowsToContents()
        #self.condautoTimer.start(1000*10)

    def automaticOrder(self):
        self.tradeTimer.stop()
        now = datetime.datetime.now()
        if self.tick == []:
            self.setCondItems()
            self.battery_check()
            self.tick.append(1)
        elif self.tick != [] and sum(self.tick) <3:
            self.tick.append(1)
        elif sum(self.tick) == 3:
            self.tick = []
        else:
            print("setting cond list error")
        print(now)

        try:
            if (self.algoStart):# and int(automaticOrderTime) >= 900 and int(automaticOrderTime)<=1530):
                if self.kiwoom.cash >= 1000:
                    self.buy_scenario()
                else:
                    pass

                if self.holdings != []:
                    self.sell_scenario()
                else:
                    pass
                print("---------------------------------")
            else:
                pass
        except Exception as e:
            print(e)
            self.pusher.send_message(message="error in pytrade "+ str(e))
            time.sleep(60)
    
        self.tradeTimer.start(1000*15.5)

    def setCondItems(self):
        #self.condautoTimer.stop()
        self.inquiryBalance()
        automaticOrderTime = QTime.currentTime().toString("hhmm")
        automaticOrderTime = int(automaticOrderTime)
        #self.cybos.update_sqlite()
            #name = "Invincible" #Power
            #index = 2  #0
        self.getConditionItems()
        stocks = self.kiwoom.condList
        #sqlres = self.sqlite.get_items()
        #for item in sqlres:
        #    stocks.append(item)
        #    stocks = list(set(stocks))

        if stocks == []:
            print('condList is empty')
            self.condItems=stocks
            return

        #tmp = {}
        #for item in stocks:
        #    val = self.cybos.get_vol_value('A'+item)
        #    tmp[item] = val
        #    sorted_tmp = sorted(tmp.items(),key=operator.itemgetter(1), reverse=True)
        #    stocks = []

        print("CondItems :",stocks)
        self.condItems = stocks
        #self.condautoTimer.start(1000*61)


    def buy_scenario(self):
        print("\n Bid Session")
        str_time = QTime.currentTime().toString("hhmm")
        time = int(str_time)
        for item in self.condItems:
            stock = 'A'+item
            series = [60,30,10,3,1]
            for min_t in series:
                df = self.cybos.stockChart(stock)
                drift = self.calc.get_drift(df.iloc[-50:])
                if drift < 0 :
                    continue
                res = self.calc.get_indis(df)
                sigma = res[6]
                if sigma < self.calc.get_baseline(price=df['close'].iloc[-1], type ='u'):
                    break 
            rsi = self.calc.RSI(df['close'])
            if rsi.iloc[-1][0]<rsi.iloc[-2][0]: #or rsi.iloc[-1][0]>77:
                continue
            res = self.calc.get_indis(df)
            _7mean = res[0]
            _20mean = res[1]
            _vol = res[2]
            delta = res[3]
            zscore = res[4]
            zscore_ch = res[5]
            zscore_n = res[7]
            sigma = res[6]
            baseline = self.calc.get_baseline(df['close'].iloc[-1])
            _delta1 = delta[0]-delta[1]
            _delta2 = delta[1]-delta[2]
            if sigma < baseline:
                continue
            if sum(zscore.iloc[-4:]) == 0: #or sum(zscore_ch.iloc[-4:]) == 0:
                continue
            if self.calc.check_ewm(m7=_7mean, m20=_20mean) == 'r':
                continue
            if self.calc.check_ewm(m7=_7mean, m20=_20mean) == 'y' and df['volume'].iloc[-1]<df['volume'].iloc[-2]*2.9:
                continue
            if _vol.iloc[-1]<_vol.iloc[-2] and _vol.iloc[-2]<_vol.iloc[-3]:
                continue
            ea = 10
            if min_t == 1:
                ea = 30

            if df['amount'].rolling(window=ea).mean().iloc[-1]/100000000 < 1: #present volume might 0 when starts
                continue

            double_loop= True
            for i in range(-1,-5,-1):
                if zscore.iloc[i]<-1.96 or zscore_ch.iloc[i]<-1.96 or zscore_n.iloc[i]>2.575:
                    double_loop = False
                    break
            if double_loop == False:
                continue
            msg = str(min_t)+"min -- "+item+" - delta : "+str(delta[0])+" z-score re: "+str(zscore.iloc[-1])+" z-score ch: "+str(zscore_ch.iloc[-1])+\
                " sigma : "+str(sigma) + " drift : "+str(drift)+ " noise : "+str(zscore_n.iloc[-1])
            print(msg)

            if df['close'].iloc[-1]>self.kiwoom.cash:
                continue

            if delta[0]>delta[1] and delta[0]>0 and _delta1>_delta2 and zscore.iloc[-1]<3: #and time<1450:
                if _delta2 <0 and _delta1<abs(_delta2):
                    continue
                self.sendOrder(1,stock[1:])
                self.pusher.send_message(message="bid "+msg)                

 
    def sell_scenario(self):
            print("\n Ask Session")
            if self.holdings ==[]:
                return
            else:
                for i in range(len(self.holdings)):
                    item = self.holdings[i]['stockCode']
                    _yeild=float(self.holdings[i]['yeild'])            
                    str_time = QTime.currentTime().toString("hhmm")
                    time = int(str_time)
                    lv = self.cybos.get_change(item)
                    if lv>0.297: #lv -100 situation
                        continue
                    r_series = [1,3,10,30,60]
                    for r_min in r_series:                        
                        df = self.cybos.stockChart(item, minute=r_min)
                        res = self.calc.get_indis(df)
                        sigma = res[6]
                        if sigma > self.calc.get_baseline(df['close'].iloc[-1], type = 'l'):
                            break
                    if df['volume'].iloc[-1]<=0:
                        continue
                    res = self.calc.get_indis(df)
                    _7mean = res[0]
                    _20mean = res[1]
                    _vol = res[2]
                    delta = res[3]
                    zscore = res[4]
                    zscore_ch = res[5]
                    zscore_n = res[7]
                    sigma = res[6]
                    _delta1 = delta[0]-delta[1]
                    _delta2 = delta[1]-delta[2]
                    msg = str(r_min)+"min -- "+ item+" - delta : "+str(delta[0])+" z-score re: "+str(zscore.iloc[-1])+" z-score ch: "+str(zscore_ch.iloc[-1])+" sigma: "+str(res[6])
                    print(msg)
                    if delta[0]<=0 and _delta1<0 and zscore.iloc[-1]<-1.96 and _vol.iloc[-1]>_vol.iloc[-2]:
                        self.stockQty = self.holdings[i]['stockQty']
                        self.sendOrder(2, item[1:])
                        self.pusher.send_message(message="ask1 "+ msg)

                    if _delta1<0 and zscore_ch.iloc[-1]<-2.575:#0.01
                        self.stockQty = self.holdings[i]['stockQty']
                        self.sendOrder(2, item[1:])
                        self.pusher.send_message(message="ask3_hard "+ msg)

                    elif delta[0]<res[6] and _delta1<0 and zscore_ch.iloc[-1]<-1.96 and _vol.iloc[-1]>_vol.iloc[-2]:
                        self.stockQty = self.holdings[i]['stockQty']
                        self.sendOrder(2, item[1:])
                        self.pusher.send_message(message="ask3 "+ msg)
                   
                    elif _delta1<0 and delta[0]<=(1/(100*np.log10(res[6]-res[6]/5))):
                        self.stockQty = self.holdings[i]['stockQty']
                        self.sendOrder(2, item[1:])
                        self.pusher.send_message(message="ask2 "+ msg)

                    elif _delta1<0 and zscore.iloc[-1]<-2.575:
                        self.stockQty = self.holdings[i]['stockQty']
                        self.sendOrder(2, item[1:])
                        self.pusher.send_message(message="ask1 hard "+ msg)
                        
                    elif delta[0]<0 and self.calc.check_ewm(m7=_7mean, m20=_20mean) == 'r':
                        self.stockQty = self.holdings[i]['stockQty']
                        self.sendOrder(2, item[1:])
                        self.pusher.send_message(message="ask5 ewm "+ msg) 
                    else:
                        pass
                       

    def flipAuto(self):
        if self.isAutomaticOrder == False:
            self.isAutomaticOrder = True
            self.logTextEdit.append("Auto trade on")
            #for test
            self.inquiryBalance()
            self.buy_scenario()
            self.sell_scenario()

        else:
            self.isAutomaticOrder = False
            self.logTextEdit.append("Auto trade off")

    def battery_check(self):
        battery = psutil.sensors_battery()
        if battery.percent < 10:
            self.pusher.send_message(message="Low Batteries")
        else:
            pass

if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWindow = MyWindow()
    sys.exit(app.exec_())